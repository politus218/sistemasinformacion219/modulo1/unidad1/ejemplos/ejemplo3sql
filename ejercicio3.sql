﻿-- Ejemplo3 proyecto en Sql del ejercicio3

-- Se crea la primera de las 5 tablas que se han de hacer.


/* Creamos la tabla profesor */

  CREATE TABLE IF NOT EXISTS profesor (
    dni varchar (10),
    nombre varchar (100) ,
    direccion varchar (100),
    tfno varchar (9),
    PRIMARY KEY(dni)
    );


 /* Creamos la tabla modulo*/

   CREATE TABLE IF NOT EXISTS modulo (
    cod int AUTO_INCREMENT,
    nombre varchar (100) ,
    PRIMARY KEY(cod)
    );


  /* Creamos la tabla imparte */

   CREATE TABLE IF NOT EXISTS imparte (
    dniprofesor varchar (9),
    codmodulo int ,    

    PRIMARY KEY(dniprofesor,codmodulo),
    UNIQUE KEY (codmodulo),
    CONSTRAINT fkimpartenprofesor FOREIGN KEY (dniprofesor) REFERENCES profesor (dni),
    CONSTRAINT fkimpartenmodulo FOREIGN KEY (codmodulo) REFERENCES modulo (cod)
    );

  /* Creamos la tabla alumno */

CREATE TABLE IF NOT EXISTS alumno (
    expediente varchar (10),
    nombre varchar (100) ,
    apellidos varchar (100),
    fechanac date,
    PRIMARY KEY(expediente)
    );

  /* Creamos la tabla esdelegado */

CREATE TABLE IF NOT EXISTS esdelegado (
    estudiante varchar (10),
    delegado varchar (10),
    
    PRIMARY KEY(estudiante, delegado),
    UNIQUE KEY (estudiante),
    CONSTRAINT fkdelegadoconalumno FOREIGN KEY (estudiante) REFERENCES alumno (expediente),
    CONSTRAINT fkdelegadoconalumno1 FOREIGN KEY (delegado) REFERENCES alumno (expediente)

    );

CREATE TABLE IF NOT EXISTS cursa (
    expedientealumno varchar (10),
    codmodulo int,
    PRIMARY KEY(expedientealumno, codmodulo),
    CONSTRAINT fkcursan FOREIGN KEY (expedientealumno) REFERENCES alumno (expediente),
    CONSTRAINT fkimparten FOREIGN KEY (codmodulo) REFERENCES modulo (cod)
    );
